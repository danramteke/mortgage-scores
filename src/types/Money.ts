export class Money {
    public amount: Number;

    public toString = () : string => {
        const n = this.amount.toFixed(2);
        return `$` + n.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    } 

    public adding = (other: Money) : Money => {
        return new Money(Number(this.amount) + Number(other.amount));
    }

    public constructor(input: any) {
        this.amount = new Number(input);
    }

}