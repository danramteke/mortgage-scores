import { Money } from "./Money";


export class Account {
    id: String;
    picture: String;
    credit: number;
    balance: Money;
    nameFirst: string;
    nameLast: string;
    employer: string;
    email: string;
    phone: string;
    address: string;
    comments: string;
    created: Date;
    tags: [string];

    public constructor(input: any) {
        this.id = input["id"];
        this.picture = input["picture"]
        this.credit = input["credit"]
        this.balance = new Money(input["balance"])
        this.address = input.address;
        this.phone = input.phone;
        this.email = input.email;
        this.employer = input.employer;

        this.nameFirst = input.name_first;
        this.nameLast = input.name_last;

        this.created = new Date(input.created)
        this.tags = input["tags"];
        this.comments = input.comments;
    }

    public name(): string {
        return this.nameFirst + " " + this.nameLast;
    }

    public rating(): Number {
        let count = 0;

        if ( this.credit >= 500 ) {
            count += 1
        }

        if ( this.balance.amount >= 1000 ) {
            count += 1;
        }

        if ( this.nameLast.length < 8 ) {
            count += 1;
        }

        if ( this.employer.length > 8 ) {
            count += 1;
        }

        if ( this.tags.length > 2 ) {
            count += 1;
        }

        return count
    }
}
