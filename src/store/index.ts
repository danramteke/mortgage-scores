import { createStore } from 'vuex'
import { Account } from '../types/Account'
import axios from "axios";

export default createStore({
  state: {
    accounts: null
  },
  mutations: {
    deliver (state, newAccounts) {
      state.accounts = newAccounts;
    }
  },
  actions: {
    load ({ commit }) {

      axios
      .get("/accounts.jsonl", {
        responseType: "text",
      })
      .then((response) => {
        const _accounts = response.data.split("\n").map((line: string) => {
          const inputAccount = JSON.parse(line);
          const account = new Account(inputAccount);
          return account;
        });

        commit('deliver', _accounts)
      });

    
    },  
  },

})
